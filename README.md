
# VS test

## Run Locally

Clone the project

```bash
git clone https://gitlab.com/lehazavalin95/vs_test
```

Go to the project directory

```bash
cd vs_test
```

Copy .env.example

```bash
cp .env.example .env
```

Fill SECRET_KEY

Run docker

```bash
docker-compose up -d
```

Migrate

```bash
docker exec -t <web container id> python manage.py migrate
```

Create superuser

```bash
docker exec -it <web container id> python manage.py createsuperuser
```

#### root

```http
  GET /api
```
