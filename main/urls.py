from django.urls import include, path

from rest_framework import routers

from .api import views

from rest_framework.authtoken import views as drf_views


router = routers.DefaultRouter()
router.register('photos', views.PhotoViewSet, basename='photo')
router.register('users', views.UserViewSet, basename='user')
router.register(
    'geolocations',
    views.GeoLocationViewSet,
    basename='geolocation'
)

urlpatterns = [
    path('', include(router.urls)),
    path('api-token-auth/', drf_views.obtain_auth_token),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
]
