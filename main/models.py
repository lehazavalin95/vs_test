from django.utils import timezone

from django.db import models
from django.utils.translation import gettext_lazy as _

from authapp.models import User


def upload_to(instance, filename):
    '''
    :param instance: Photo instance
    :param filename: filename of image field
    :return: path to image of Photo instance
    '''
    today = timezone.now()
    year = today.strftime('%Y')
    month = today.strftime('%m')
    day = today.strftime('%d')
    return '/'.join([instance.user.username, year, month, day, filename])


class GeoLocation(models.Model):
    name = models.CharField(
        verbose_name=_('Название места'),
        max_length=40
    )
    latitude = models.DecimalField(
        verbose_name=_('Широта'),
        max_digits=9,
        decimal_places=6
    )
    longitude = models.DecimalField(
        verbose_name=_('Долгота'),
        max_digits=9,
        decimal_places=6
    )

    class Meta:
        verbose_name = _('Геолокация')
        verbose_name_plural = _('Геолокации')

    def __str__(self):
        return self.name


class Photo(models.Model):
    image = models.ImageField(
        upload_to=upload_to,
        verbose_name=_('Фото')
    )
    description = models.TextField(
        verbose_name=_('Описание'),
        null=True,
        blank=True
    )
    user = models.ForeignKey(
        verbose_name=_('Пользователь'),
        to=User,
        on_delete=models.CASCADE,
        related_name='photos'
    )
    users = models.ManyToManyField(
        verbose_name=_('Люди на фото'),
        to=User,
        related_name='photo_users'
    )
    geolocation = models.ForeignKey(
        to=GeoLocation,
        verbose_name=_('Геолокация'),
        on_delete=models.SET_NULL,
        null=True
    )
    date_created = models.DateTimeField(
        verbose_name=_('Дата загрузки'),
        auto_now_add=True
    )

    class Meta:
        verbose_name = _('Фото')
        verbose_name_plural = _('Фото')

    def __str__(self):
        return self.image.name
