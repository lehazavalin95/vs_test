from django.contrib import admin

from .models import GeoLocation, Photo


@admin.register(GeoLocation)
class GeoLocationAdmin(admin.ModelAdmin):
    pass


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    pass
