from rest_framework import viewsets

from django_filters import rest_framework as filters, DateFilter

from django.db import models

from main.models import Photo, GeoLocation
from main.api import serializers
from authapp.models import User


class PhotoFilter(filters.FilterSet):
    class Meta:
        model = Photo
        fields = {
            'date_created': ('gte', 'lte', ),
            'users__first_name': ('icontains', ),
            'geolocation__name': ('iexact', )
        }

    filter_overrides = {
        models.DateTimeField: {
            'filter_class': DateFilter
        },
    }


class GeoLocationViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.GeoLocationSerializer
    queryset = GeoLocation.objects.all()


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()


class PhotoViewSet(viewsets.ModelViewSet):
    '''
    Список фото
    '''
    search_fields = ['users__first_name']
    filter_class = PhotoFilter

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.PhotoListSerializer
        if self.action == 'retrieve':
            return serializers.PhotoDetailSerializer
        if self.action == 'create':
            return serializers.PhotoCreateSerializer
        return serializers.PhotoListSerializer

    def get_queryset(self):
        if self.action == 'retrieve':
            return Photo.objects.prefetch_related('users')\
                .select_related('geolocation')
        return Photo.objects.all()
