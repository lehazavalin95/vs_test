from main import models

from rest_framework import serializers

from django.contrib.auth.password_validation import validate_password


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True,
                                     validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = models.User
        fields = ['id', 'first_name', 'last_name', 'username', 'password',
                  'password2']

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {'password': 'Пароли не совпадают'})

        return attrs

    def create(self, validated_data):
        user = models.User.objects.create(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class PhotoListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Photo
        fields = ['id', 'image']


class GeoLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GeoLocation
        fields = ['name', 'latitude', 'longitude']


class PhotoCreateSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = models.Photo
        fields = ['user', 'image', 'description', 'geolocation', 'users']


class PhotoDetailSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True, read_only=True)
    geolocation = GeoLocationSerializer(read_only=True)

    class Meta:
        model = models.Photo
        fields = ['image', 'description', 'geolocation', 'users']
